using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public bool InputKeyCodeDown;
    public GameObject Phonarik;
    public GameObject TextAboutPhonarik;
    // Start is called before the first frame update
    void Start()
    {
        Phonarik.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (InputKeyCodeDown == false)
            {
                Phonarik.SetActive(true);
                InputKeyCodeDown = true;
                //Debug.Log("1");
            }
            else if (InputKeyCodeDown == true)
            {
                Phonarik.SetActive(false);
                InputKeyCodeDown = false;
                //Debug.Log("0");
            }
        }
    }
}
