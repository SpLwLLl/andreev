using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class TriggerDetectScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float TimerTest;
    public bool TakeDamageValue;
    public int HP;
    public TMP_Text ZdorovieText;

    void Start()
    {
        ZdorovieText.text = ZdorovieText.text = ((int)HP).ToString();
    }

    private void ObnovitHpDisplay()
    {
        ZdorovieText.text = ZdorovieText.text = ((int)HP).ToString();
    }
    void Update()
{
        TimerTest += Time.deltaTime;
    if (TimerTest >= 1 & TakeDamageValue == true)
    {
        TimerTest = 0;
        HP -= 5;
        Debug.Log(HP);
        ObnovitHpDisplay();
    }
    else if (HP > 100)
        {
            HP = 100;
           ObnovitHpDisplay();
        }
    else if (HP < 0)
        {
            HP = 0;
            ObnovitHpDisplay();
        }

}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Zone")
        {
            TakeDamageValue = true;
            //Debug.Log("Player has entered Zone");
        }
        else if (other.gameObject.tag == "Yabloko")
        {
            HP += 15;
            Destroy(other.gameObject);
            ObnovitHpDisplay();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Zone")
        {
        TakeDamageValue = false;
            //Debug.Log("Player has left Zone");
        }
    }
}

