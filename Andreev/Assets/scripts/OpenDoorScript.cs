using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorScript : MonoBehaviour
{
    public GameObject Button;
    public GameObject Door;
    public GameObject Monetka;
    public Transform Spawn;

    public bool Inside;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Instantiate(Monetka, Spawn.position, Quaternion.identity);
            Inside = true;
            Door.SetActive(false);
        }
        else if (other.gameObject.tag == "Cube")
        {
            Instantiate(Monetka, Spawn.position, Quaternion.identity);
            Inside = true;
            Door.SetActive(false);
        }
    }

    // Update is called once per frame
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            
            Inside = false;
            Door.SetActive(true);
        }
        else if (other.gameObject.tag == "Cube")
        {
            Inside = false;
            Door.SetActive(true);
        }
    }
}
