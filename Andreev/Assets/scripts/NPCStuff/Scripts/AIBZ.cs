using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBZ : MonoBehaviour
{
    public Transform Player;
    float distance;
    NavMeshAgent myAgent;
    Animator myAnim;

    private void Awake()
    {
        if (Player == null)
        {
            Player = GameObject.FindWithTag("Player").transform;
        }
    }

    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(transform.position, Player.position);
        Debug.Log(distance); 
        if (distance >= 20) 
        {
            Debug.Log("Idle");
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", true);
            myAnim.SetBool("ATTACK", false);
            myAnim.SetBool("RUN", false);
   
        }
        if (distance >= 10) 
        {
            Debug.Log("Detect and run");
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTACK", false);
            myAnim.SetBool("RUN", true);
            
        }
        if (distance <= 2)
        {
            Debug.Log("Attack");
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTACK", true);
            myAnim.SetBool("RUN", false);
       
        }

    }
}
