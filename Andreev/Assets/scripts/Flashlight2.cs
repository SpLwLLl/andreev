using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flashlight2 : MonoBehaviour
{
    public bool InputKeyCodeDown2;
    public GameObject Phonarik2;
    public Text TextFlashlight;
    // Start is called before the first frame update
    void Start()
    {
        Phonarik2.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) & InputKeyCodeDown2 == false)
        {
            TextFlashlight.text = "Flashlight: On";
            TextFlashlight.color = Color.green;
            Phonarik2.SetActive(true);
            InputKeyCodeDown2 = true;
        }
        else if (Input.GetKeyDown(KeyCode.F) & InputKeyCodeDown2 == true)
        {
            TextFlashlight.text = "Flashlight: Off";
            TextFlashlight.color = Color.black;
            Phonarik2.SetActive(false);
            InputKeyCodeDown2 = false;
        }
    }
}