using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aptechka : MonoBehaviour
{
    public GameObject Regen;

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            //Debug.Log("Player has entered Zone");
            Destroy(Regen);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
